var toggleproxy = {

	consoleService: Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService), 
	prefManager: Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch),
	prefs: Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("extensions.toggleproxy."),
    language: Components.classes["@mozilla.org/intl/stringbundle;1"].getService(Components.interfaces.nsIStringBundleService).createBundle("chrome://toggleproxy/locale/toggleproxy.properties"),

	// Called at various times, updates the icon in the status bar.
	// Decided to get the preference setting again to check that changes 
	// have taken place.
	updateIcon : function() 
	{
		// Update the icon on the status bar
		
		// Get proxy status
		var proxyStatus = this.prefManager.getIntPref("network.proxy.type");
		
		// Grab proxy icon
		var proxyButton = document.getElementById("toggleproxy-status");
		
		// Grab toolbar icon
		var proxyToolbarButton = document.getElementById("toggleproxy-toolbar-button");

		if (proxyStatus==0)  // No proxy
		{
			proxyButton.setAttribute("tooltiptext", this.language_tooltip_noproxy);
			proxyButton.setAttribute("class", "toggleproxy-status-noproxy");
			if (proxyToolbarButton != null)	// toolbar button activated, if not result will be null
			{
				proxyToolbarButton.setAttribute("tooltiptext", this.language_tooltip_noproxy);  
				proxyToolbarButton.setAttribute("state", "no");
			}
			
		} 
		else if (proxyStatus==1)  // Manual proxy
		{
			proxyButton.setAttribute("tooltiptext", this.language_tooltip_manual);
			proxyButton.setAttribute("class", "toggleproxy-status-manualproxy");
			if (proxyToolbarButton != null)	// toolbar button activated, if not result will be null
			{
				proxyToolbarButton.setAttribute("tooltiptext", this.language_tooltip_manual);  
				proxyToolbarButton.setAttribute("state", "manual");
			}
		}
		else if (proxyStatus==4)  // Auto-detect proxy
		{
			proxyButton.setAttribute("tooltiptext", this.language_tooltip_auto);
			proxyButton.setAttribute("class", "toggleproxy-status-autoproxy");
			if (proxyToolbarButton != null)	// toolbar button activated, if not result will be null
			{
				proxyToolbarButton.setAttribute("tooltiptext", this.language_tooltip_auto);  
				proxyToolbarButton.setAttribute("state", "auto-detect");
			}
		}
		else if (proxyStatus==5)  // System proxy
		{
			proxyButton.setAttribute("tooltiptext", this.language_tooltip_system);
			proxyButton.setAttribute("class", "toggleproxy-status-systemproxy");
			if (proxyToolbarButton != null)
			{
				proxyToolbarButton.setAttribute("tooltiptext", this.language_tooltip_system);  
				proxyToolbarButton.setAttribute("state", "system");
			}
		}
		else if (proxyStatus==2)  // Auto proxy (URL)
		{
			proxyButton.setAttribute("tooltiptext", this.language_tooltip_autourl);
			proxyButton.setAttribute("class", "toggleproxy-status-autourl");
			if (proxyToolbarButton != null)	// toolbar button activated, if not result will be null
			{
				proxyToolbarButton.setAttribute("tooltiptext", this.language_tooltip_autourl);  
				proxyToolbarButton.setAttribute("state", "auto-proxy");
			}
		}
	},
	
	// Called when Firefox loads
	onLoad : function() 
	{
		if (this.prefs.getBoolPref("debug"))
		{
			this.consoleService.logStringMessage("DEBUG: onLoad");
		}		

		// get locale display information 
        this.language_preferences_title = this.language.GetStringFromName("toggleproxy_preferencess_title");
        this.language_tooltip_auto = this.language.GetStringFromName("toggleproxy_tooltip_auto");
        this.language_tooltip_noproxy = this.language.GetStringFromName("toggleproxy_tooltip_noproxy");
        this.language_tooltip_manual = this.language.GetStringFromName("toggleproxy_tooltip_manual");
        this.language_tooltip_system = this.language.GetStringFromName("toggleproxy_tooltip_system");
        this.language_tooltip_autourl = this.language.GetStringFromName("toggleproxy_tooltip_autourl");

		this.updateIcon();
	},

	
	// Called on icon click
    onClick : function(e) 
    {    
		if (this.prefs.getBoolPref("debug"))
		{
			this.consoleService.logStringMessage("DEBUG: onClick: button " + e.button);
		}		

		if (e.button == 0) { this.toggleProxy(); }   // left click
		if (e.button == 2) { this.openPreferences(); }   // right click
		e.preventDefault();
    },

	// Called on toolbar click
    onToolbar : function(e) 
    {    
		if (this.prefs.getBoolPref("debug"))
		{
			this.consoleService.logStringMessage("DEBUG: onToolbar pressed " + e);
		}
		
		this.toggleProxy();
		e.preventDefault();
    },

	// open the preferences window
	openPreferences : function() 
	{
		try 
		{
			var features = "chrome,titlebar,toolbar,centerscreen,modal,dependent";
			window.openDialog('chrome://toggleproxy/content/toggleproxyPref.xul', this.language_preferences_title, features);
		} 
		catch (rErr) // catch and display any errors
		{
			alert(rErr);
		}
	},

	// Toggle between the two proxies stored in the preferences
	toggleProxy : function()
	{
		// Get proxy status
		var proxyStatus = this.prefManager.getIntPref("network.proxy.type");
		var optionOne = this.prefs.getIntPref('toggleone');
		var optionTwo = this.prefs.getIntPref('toggletwo');
		var optionThree = this.prefs.getIntPref('togglethree');
		var modified = false;
		
		if (this.prefs.getBoolPref("debug"))
		{
			this.consoleService.logStringMessage("DEBUG: toggleProxy");
		}		

		// check first toggle, if it isn't set to toggle one set it to that value
		if (proxyStatus == optionOne)   
		{					 
			this.prefManager.setIntPref("network.proxy.type", optionTwo);
			modified = true;
		}
		else if (proxyStatus == optionTwo)  // current setting same as toggle one, set to toggle two.
		{
			this.prefManager.setIntPref("network.proxy.type", optionThree);
			modified = true;
		}
		else if (proxyStatus == optionThree)  // current setting same as toggle one, set to toggle two.
		{
			this.prefManager.setIntPref("network.proxy.type", optionOne);
			modified = true;
		}

		// Only update the icon if we've changed the proxy type		
		if (modified)
		{
			this.updateIcon();
		}
	},
};	
	
	
	
