// debug messages available
pref("extensions.toggleproxy.debug", false);

// toggle one default proxy setting = No proxy
pref("extensions.toggleproxy.toggleone", 0);

// toggle two default proxy setting = Auto proxy (URL)
pref("extensions.toggleproxy.toggletwo", 2);

// toggle two default proxy setting = Manual proxy
pref("extensions.toggleproxy.togglethree", 1);

